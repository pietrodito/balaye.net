---
title: Le syndrome de Stockholm - vous allez adorer les macros SAS - Partie 3
date: "2022-09-11T00:00:00Z"
summary: Deux macros pour nettoyer les données PMSI.
featured_image: 'cover.jpg'
omit_header_text: true
---

Voici deux macros indispensables pour le travail sur le PMSI :

```sql
%macro nettoyage_sejours(prefixe);
AND &prefixe..ETA_NUM
             not in (
             '130780521', '130783236', '130783293', '130784234',
             '130804297', '600100101', '750041543', '750100018',
             '750100042', '750100075', '750100083', '750100091',
             '750100109', '750100125', '750100166', '750100208',
             '750100216', '750100232', '750100273', '750100299',
             '750801441', '750803447', '750803454', '910100015',
             '910100023', '920100013', '920100021', '920100039',
             '920100047', '920100054', '920100062', '930100011',
             '930100037', '930100045', '940100027', '940100035',
             '940100043', '940100050', '940100068', '950100016',
             '690783154', '690784137', '690784152', '690784178',
             '690787478', '830100558')
  AND &prefixe..GRG_GHM not like '90%'
  AND (&prefixe..SEJ_TYP is null OR &prefixe..SEJ_TYP <> 'B')
%mend;
```

```sql
%macro nettoyage_chainage(prefixe);
  AND (&prefixe..NIR_RET = '0' AND &prefixe..NAI_RET = '0' AND
       &prefixe..SEX_RET = '0' AND &prefixe..SEJ_RET = '0' AND
       &prefixe..FHO_RET = '0' AND &prefixe..PMS_RET = '0' AND
       &prefixe..DAT_RET = '0')
%mend;
```
